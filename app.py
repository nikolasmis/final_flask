import numpy as np
import pandas as pd
import flask
from flask import render_template
import pickle
from sklearn.tree import DecisionTreeRegressor

app = flask.Flask(__name__, template_folder='templates')

@app.route('/', methods=['POST', 'GET'])
def main():
   
    if flask.request.method == 'GET':
        return render_template('main.html')

    
    if flask.request.method == 'POST':

        
        with open('desc.pkl', 'rb') as F:
            loaded_model = pickle.load(F)

        with open('X_names.pkl', 'rb') as F:
            X_columns, y_columns = pickle.load(F)

        
        iw_value = int(flask.request.form['IW'])   
        if_value = int(flask.request.form['IF'])   
        vw_value = float(flask.request.form['VW']) 
        fp_value = int(flask.request.form['FP'])   
        submit_value = flask.request.form['predict_btn']
        
     
        welding_params = []
        welding_params.append(iw_value)
        welding_params.append(if_value)
        welding_params.append(vw_value)
        welding_params.append(fp_value)
        X = pd.DataFrame([welding_params], columns=X_columns)

        predictions = loaded_model.predict(X)
        y = pd.DataFrame(predictions, columns=y_columns)

        width = round(y.Width.values[0], 3)
        depth = round(y.Depth.values[0], 3)

        data = {
            'iw_value': iw_value,
            'if_value': if_value,
            'vw_value': vw_value,
            'fp_value': fp_value,
            'width':  width,
            'depth': depth,
            'submit': submit_value
        }
        
        return render_template('main.html', result = data)

if __name__ == '__main__':
    app.run(debug=True)